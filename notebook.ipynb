{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Hydrogeodesy: Monitoring surface waters from space\n",
    "### Exercise 2: Wide-Swath-Altimetry\n",
    "\n",
    "Daniel Scherer, DGFI-TUM  \n",
    "Wintersemester 2024/25\n",
    "\n",
    "##### **Contents**\n",
    "1. Introduction to the SWOT Pixel Cloud (PixC) product\n",
    "2. Estimating lake water levels from SWOT PixC data\n",
    "3. Bonus: Estimating river water levels and slope from SWOT\n",
    "\n",
    "\n",
    "In this exercise, we will use the SWOT Pixel Cloud (PixC) product to estimate water levels.\\\n",
    "The SWOT PixC product is a Level 2 product, containing a point cloud of georeferenced water level estimates derived from interferometric radar measurements.\\\n",
    "This exercise builds on Exercise 1 and we will investigate the same target (Sam Rayburn Reservoir in Texas).\n",
    "\n",
    "#### 2.1 Introduction to the SWOT Pixel Cloud (PixC) product\n",
    "\n",
    "SWOT PixC data is provided by Cycles, Passes, and Tiles.\\\n",
    "Each tile covers an area of approximately 64 x 64 km, containing a point cloud with hundrets of MB of data.\\\n",
    "The Sam Rayburn Reservoir is covered by muiliple SWOT Pixel Cloud tiles of the ascending pass 578 and the ascending pass 259 as shown in Figure 1.\\\n",
    "\n",
    "<img src=\"Figures/SWOT.png\" alt=\"Map\" style=\"width: 200mm;\"/>\\\n",
    "*Figure 1: Sam Rayburn Reservoir and the approximate footprints of the intersecting tiles of passes 259 and 578.*\n",
    "\n",
    "To minimize data volume, we gathered the PixC data for the Sam Rayburn Reservoir in a single file from all tiles per pass and cycle.\\\n",
    "The gathered data contains only a subset of all the available variables in the PixC data:\n",
    "\n",
    "**Geolocation**\n",
    "\n",
    "| **Parameter**                | **Description**                                                                 |\n",
    "|------------------------------|---------------------------------------------------------------------------------|\n",
    "| **latitude**                  | Geographic latitude of each pixel's location.                                  |\n",
    "| **longitude**                 | Geographic longitude of each pixel's location.                                 |\n",
    "| **height**                    | The height above reference ellipsoid (m) |\n",
    "\n",
    "**Corrections**\n",
    "| **Parameter**                | **Description**                                                                 |\n",
    "|------------------------------|---------------------------------------------------------------------------------|\n",
    "| **instrument_phase_cor**      | Correction for phase errors caused by the radar instrument. **Already applied to height**                    |\n",
    "| **instrument_range_cor**      | Correction for errors in range measurement due to instrument inaccuracies. **Already applied to height**     |\n",
    "| **instrument_baseline_cor**   | Correction for baseline errors in radar measurements. **Already applied to height**                          |\n",
    "| **iono_cor_gim_ka**           | Ionization correction based on the GIM model, specifically for Ka-band radar. **Already applied to height** |\n",
    "| **model_dry_tropo_cor**       | Correction for dry tropospheric delay affecting radar signal propagation. **Already applied to height**      |\n",
    "| **model_wet_tropo_cor**       | Correction for wet tropospheric delay due to moisture in the atmosphere. **Already applied to height**       |\n",
    "| **height_cor_xover**          | Height correction from crossover adjustment (e.g., roll correction) (m) **Already applied to height** |\n",
    "| **solid_earth_tide**          | Correction for solid Earth tides caused by gravitational forces from the moon and sun. |\n",
    "| **pole_tide**                 | Correction for Earth's pole movement, affecting radar measurements.            |\n",
    "\n",
    "**Quality Indicators and Classifications**\n",
    "| **Parameter**                | **Description**                                                                 |\n",
    "|------------------------------|---------------------------------------------------------------------------------|\n",
    "| **sig0**                      | Radar bbackscatter coefficient (dB). Larger values represent more specular features while lower values represent rough surfaces. |\n",
    "| **classification**            | Water classification of the point cloud **(1: land, 2: land_near_water, 3: water_near_land, 4: open_water, 5: dark_water, 6: low_coh_water_near_land, 7: open_low_coh_water)**               |\n",
    "| **interferogram_qual**        | Quality flag assessing the reliability of the interferogram data.           |\n",
    "| **geolocation_qual**          | Quality flag indicating the accuracy of the pixel’s geolocation data.          |\n",
    "| **sig0_qual**                 | Quality flag for the radar backscatter coefficient, assessing data reliability. |\n",
    "| **pixc_line_qual**            | Quality flag for the specific line in the pixel cloud, indicating its reliability. |\n",
    "| **cross_track**            | Acrosstrack distance from the nadir point (m). |\n",
    "\n",
    "\n",
    "The qualityflags should all be 0, indicating that the data is reliable. Values greater than 0 indicate that the data is suspect or bad.\n",
    "\n",
    "For a detailed description you can refer to the [SWOT Level 2 HR PIXC Product Description](https://archive.podaac.earthdata.nasa.gov/podaac-ops-cumulus-docs/web-misc/swot_mission_docs/pdd/D-56411_SWOT_Product_Description_L2_HR_PIXC_20231026_RevBcite.pdf).\n",
    "\n",
    "**First, import the required libaries**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import xarray as xr # for reading in netcdf files\n",
    "import matplotlib.pyplot as plt # for plotting\n",
    "import numpy as np # for array operations\n",
    "import pandas as pd\n",
    "from res import labels, classification_colors, norm, cmap, examples\n",
    "from pathlib import Path"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Take a look at the following 4 examples:**\n",
    "\n",
    "<img src=\"Figures/Example1.png\" alt=\"Example1\" style=\"width: 300mm;\"/>\n",
    "<img src=\"Figures/Example2.png\" alt=\"Example2\" style=\"width: 300mm;\"/>\n",
    "<img src=\"Figures/Example3.png\" alt=\"Example3\" style=\"width: 300mm;\"/>\n",
    "<img src=\"Figures/Example4.png\" alt=\"Example4\" style=\"width: 300mm;\"/>\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note, that we already removed points with the classes \"Land\" and \"Land near Water\".\n",
    "\n",
    "Although the 2D SWOT data is a hugh improvement compared to the classical nadir altimetry, it still contains some imperfect data, which must be removed before calculating the water surface elevation.\n",
    "\n",
    "**Out of the four examples, identify the following states and discuss their reasons:**\n",
    "- Insufficient surface roughness\n",
    "- Good example\n",
    "- Missing roll correction\n",
    "- Exceeding the nominal observation limit\n",
    "\n",
    "**Now load the dataset (ds) of Example 4:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_root = Path('data/SamRayburn/PixC/')\n",
    "filename = next(data_root.glob(f'SWOT_L2_HR_PIXC_{examples[3][\"cycle_nr\"]}_{examples[3][\"pass_nr\"]}_*.nc'))\n",
    "ds = xr.open_dataset(filename, engine='netcdf4')\n",
    "ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We access the values stored in the dataset as vectors similar to last exercise, e.g.:\n",
    "\n",
    "```python\n",
    "new_height = ds.height + 1 # Increase the value of all heights by 1\n",
    "```\n",
    "\n",
    "We can create a boolean vector by testing the data against a condition:\n",
    "```python\n",
    "ds.height > ds.height.mean() # Returns: array([False, False, False, ...,  True,  True,  True])\n",
    "```\n",
    "\n",
    "We can combined multiple conditions using the & (and) and | (or) operators:\n",
    "```python\n",
    "(ds.height > ds.height.mean()) & ((ds.classification == 4) | (ds.classification == 3)) # Returns: array([False, False, False, ...,  False,  False,  True])\n",
    "```\n",
    "\n",
    "**Now, based on the examples above, create a boolean mask vector that marks all outlier as False and all good data as True**\\\n",
    "- You will need the ds.classification, ds.height_cor_xover, and ds.cross_track values\n",
    "- For the cross_track outliers you can either use the | parameter or use np.abs(ds.cross_track) to use absolute values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "quality_flags = (ds.interferogram_qual == 0) & (ds.geolocation_qual == 0) & (ds.sig0_qual == 0) # We already mask out data which are not valid according to the quality flags\n",
    "\n",
    "mask = quality_flags & ... # Add your additional outlier criteria here\n",
    "mask"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Now we plot the data again with your outlier criteria**\\\n",
    "This can take some minutes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the data again with the outlier criteria applied\n",
    "\n",
    "fig, axs = plt.subplots(1, 4, figsize=(22, 6))\n",
    "plt.subplots_adjust(bottom=0.2)\n",
    "\n",
    "plt.sca(axs[0])\n",
    "plt.scatter(x=ds.longitude[mask], y=ds.latitude[mask], c=ds.height[mask], cmap='jet', s=1, vmin=ds.height.median().values - 1, vmax=ds.height.median().values + 1)\n",
    "plt.colorbar(location='bottom', extend='both').set_label('Height (m)')\n",
    "axs[0].axis('equal')\n",
    "\n",
    "plt.sca(axs[1])\n",
    "plt.scatter(x=ds.longitude[mask], y=ds.latitude[mask], c=ds.sig0[mask], cmap='jet', s=1, vmin=0, vmax=100)\n",
    "plt.colorbar(location='bottom').set_label('normed Sigma0')\n",
    "axs[1].axis('equal')\n",
    "\n",
    "plt.sca(axs[2])\n",
    "plt.scatter(x=ds.longitude[mask], y=ds.latitude[mask], c=ds.cross_track[mask], cmap='RdYlBu', s=1, vmin=-64000, vmax=64000)\n",
    "plt.colorbar(location='bottom').set_label('Cross Track Distance [m]')\n",
    "axs[2].axis('equal')\n",
    "\n",
    "plt.sca(axs[3])\n",
    "scatter = plt.scatter(\n",
    "    x=ds.longitude[mask],\n",
    "    y=ds.latitude[mask],\n",
    "    c=ds.classification[mask],\n",
    "    cmap=cmap,\n",
    "    norm=norm,\n",
    "    s=1\n",
    ")\n",
    "\n",
    "# Create a legend\n",
    "handles = [\n",
    "    plt.Line2D([0], [0], marker='o', color='w', markerfacecolor=color, markersize=10, label=label)\n",
    "    for label, color in zip(labels.values(), classification_colors.values())\n",
    "]\n",
    "plt.legend(handles=handles, title=\"Classification\", loc=\"upper left\", bbox_to_anchor=(1, 1))\n",
    "axs[3].axis('equal')\n",
    "plt.suptitle(f'Masked Example: {filename.stem}')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Do you think that most of the outliers are removed?\n",
    "\n",
    "#### 2.2 Estimating lake water levels from SWOT PixC data\n",
    "\n",
    "**Plot a histogram of the valid heights:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "corrected_height = ds.height - ds.pole_tide - ds.solid_earth_tide\n",
    "\n",
    "xr.plot.hist(corrected_height[mask & (corrected_height < corrected_height.quantile(0.9)) & (corrected_height > corrected_height.quantile(0.01))], bins=100, label='Histogram')\n",
    "plt.gca().axvline(corrected_height.median().values, color='r', linestyle='--', label='Median')\n",
    "plt.xlabel('Height [m]')\n",
    "plt.ylabel('Frequency')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Take a look into the folder ```Figures/MoreExamples/``` for more examples of other cycles.\n",
    "\n",
    "**What would be a better method than the median to obtain the average height?**\n",
    "\n",
    "**What important quantity other than the average height can we extract from the data?**\n",
    "\n",
    "**DAHITI uses a similar method to estimate the water height. Load and plot the time series below:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "swot_dahiti_ts = pd.read_csv('data/SamRayburn/timeseries_swot.csv',delimiter=';', parse_dates=['date'], usecols=[0, 2]).set_index('date').height\n",
    "dahiti_error_ts = pd.read_csv('data/SamRayburn/timeseries_swot.csv',delimiter=';', parse_dates=['date'], usecols=[0, 3]).set_index('date').error\n",
    "insitu_ts = pd.read_csv('data/SamRayburn/insitu_wse.csv',delimiter=' ', names=['date', 'height'], parse_dates=['date'], usecols=[0, 1]).set_index('date').height\n",
    "\n",
    "bias = (swot_dahiti_ts - insitu_ts).median()\n",
    "swot_dahiti_ts = swot_dahiti_ts - bias\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(12, 6))\n",
    "ax.errorbar(swot_dahiti_ts.index, swot_dahiti_ts, yerr=dahiti_error_ts, fmt='', label='SWOT DAHITI with uncertainty', color='b')\n",
    "insitu_ts[insitu_ts.index >= swot_dahiti_ts.index.min()].plot(ax=ax, label='InSitu', color='r')\n",
    "\n",
    "\n",
    "plt.legend()\n",
    "plt.ylabel('Height [m]')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Additionally, calculate the RMSE of the SWOT data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def calc_rmse(predictions, truth):\n",
    "    rmse = np.sqrt(((predictions - truth) ** 2).mean())\n",
    "    return rmse\n",
    "\n",
    "rmse = calc_rmse(swot_dahiti_ts, insitu_ts)\n",
    "print(f'RMSE: {rmse.round(3)} m')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**How would you describe the results compared to the nadir altimetry in the last excercise?**\n",
    "\n",
    "**What are the major benefits of the wide swath SWOT mission?**\n",
    "\n",
    "#### 2.3 Bonus: Estimating river water levels and slope from SWOT"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Load the SWOT data for a reach of the Oder River**\n",
    "\n",
    "We already isolated the river from the remaining pointcloud and the data contains only points of the class \"Open Water\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filename = 'data/Oder/SWOT_L2_HR_PIXC_020_557_20240909.nc'\n",
    "ds = xr.open_dataset(filename, engine='netcdf4')\n",
    "ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axs = plt.subplots(1,2,figsize=(12, 6))\n",
    "\n",
    "plt.sca(axs[0])\n",
    "plt.scatter(ds.longitude, ds.latitude, c=ds.height, cmap='jet', s=1, vmin=ds.height.median().values - 1, vmax=ds.height.median().values + 1)\n",
    "plt.colorbar(location='bottom', extend='both').set_label('Height [m]')\n",
    "plt.xlabel('Longitude')\n",
    "plt.ylabel('Latitude')\n",
    "plt.title('Height')\n",
    "\n",
    "plt.sca(axs[1]) \n",
    "plt.scatter(ds.longitude, ds.height)\n",
    "plt.xlabel('Longitude')\n",
    "plt.ylabel('Height [m]')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Think about a method to estimate a representive water level of the river reach that works with different .**\n",
    "\n",
    "**Estimate the slope of the reach [m/km]**\\\n",
    "For this task you would need the distance along the river.\\\n",
    "As a proxy, we can convert the longitude to meters:\n",
    "```python\n",
    "longitude_km = ds.longitude * np.cos(np.radians(52)) * 111.32 # At the equator, 1 degree longitude equals 111.32 km. We have to transform the value for 52 degree latitude.\n",
    "```\n",
    "\n",
    "You might need the [np.polyfit](https://numpy.org/doc/stable/reference/generated/numpy.polyfit.html) function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "longitude_km = ds.longitude * np.cos(np.radians(52)) * 111.32\n",
    "\n",
    "# Add your code here"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "base",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
