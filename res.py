import matplotlib.colors as mcolors

classification_colors = {
    1: "brown",
    2: "orange",
    3: "lightblue",
    4: "blue",
    5: "darkblue",
    6: "green",
    7: "darkgreen"
}
labels = {
    1: "Land",
    2: "Land near Water",
    3: "Water near Land",
    4: "Open Water",
    5: "Dark Water",
    6: "Low Coh Water near Land",
    7: "Open Low Coh Water"
}

norm = mcolors.BoundaryNorm(boundaries=list(classification_colors.keys()), ncolors=len(classification_colors))
cmap = mcolors.ListedColormap(classification_colors.values())

examples = [
    {'cycle_nr': '001', 'pass_nr': '578'},
    {'cycle_nr': '004', 'pass_nr': '578'},
    {'cycle_nr': '011', 'pass_nr': '578'},
    {'cycle_nr': '004', 'pass_nr': '259'},
]